package br.com.exercicios.contato.contatos.DTOs;

public class ContatoDTO {

    private String nome;

    private String telefone;

    public ContatoDTO() {
    }

    public ContatoDTO(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
