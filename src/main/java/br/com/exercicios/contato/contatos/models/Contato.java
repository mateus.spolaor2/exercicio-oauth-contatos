package br.com.exercicios.contato.contatos.models;

import javax.persistence.*;

@Entity
public class Contato {

    @Id
    @Column(name = "owner", nullable = false)
    private String proprietario;

    private String telefone;

    private String nome;

    public Contato(String proprietario, String telefone, String nome) {
        this.proprietario = proprietario;
        this.telefone = telefone;
        this.nome = nome;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Contato() {
    }
}
